# qt 5.9.4 Install Package for Raspberry PI Raspian

This was generated with https://github.com/chschnell/build-qt5-rpi, due to the long time it took to compile on my laptop I figured others could do with it too

## Usage:

Super simple to install.

Download the file to your pi

    wget https://bitbucket.org/doghousedean/qt5-opensource-rpi-deb/raw/3890a41ec4bd2f13eb948617d1e0842c06a700be/qt-everywhere-opensource-rpi_5.9.4_armhf.deb

Install with apt

    sudo apt install qt-everywhere-opensource-rpi_5.9.4_armhf.deb
